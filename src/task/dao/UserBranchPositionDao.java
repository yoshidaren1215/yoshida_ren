package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import task.beans.UserBranchPosition;
import task.exception.SQLRuntimeException;

public class UserBranchPositionDao {

	public List<UserBranchPosition> getUBP(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("users.branch as branch, ");
			sql.append("users.position as position, ");
			sql.append("users.account_state as account_state, ");
			sql.append("branchs.branch_nunber as branch_nunber, ");
			sql.append("branchs.branch_name as branch_name, ");
			sql.append("positions.position_number as position_number, ");
			sql.append("positions.position_name as position_name, ");
			sql.append("users.created_date as created_date ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branchs ");
			sql.append("ON users.branch = branchs.branch_nunber ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position = positions.position_number ");
			sql.append("ORDER BY users.id limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserBranchPosition> ret = toUBPList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchPosition> toUBPList(ResultSet rs) throws SQLException {

		List<UserBranchPosition> ret = new ArrayList<UserBranchPosition>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int position = rs.getInt("position");
				String branchName = rs.getString("branch_name");
				String positionName = rs.getString("position_name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int accountState = rs.getInt("account_state");

				UserBranchPosition management = new UserBranchPosition();
				management.setId(id);
				management.setLoginId(loginId);
				management.setName(name);
				management.setBranch(branch);
				management.setPosition(position);
				management.setBranchName(branchName);
				management.setPositionName(positionName);
				management.setCreatedDate(createdDate);
				management.setAccountState(accountState);

				ret.add(management);
			}
			return ret;
		} finally {
			close(rs);
		}

	}

}
