package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import task.beans.UserMessage;
import task.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.message_state as message_state, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int messageState = rs.getInt("message_state");

                UserMessage message = new UserMessage();
                message.setLoginId(loginId);
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);
                message.setTitle(title);
                message.setText(text);
                message.setCategory(category);
                message.setCreatedDate(createdDate);
                message.setMessageState(messageState);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public List<UserMessage> getCategory(Connection connection, String search){

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT ");
    		sql.append("messages.id as id, ");
    		sql.append("messages.title as title, ");
    		sql.append("messages.text as text, ");
    		sql.append("messages.category as category, ");
    		sql.append("messages.user_id as user_id, ");
    		sql.append("messages.message_state as message_state, ");
    		sql.append("users.login_id as login_id, ");
    		sql.append("users.name as name, ");
    		sql.append("messages.created_date as created_date ");
    		sql.append("FROM messages ");
    		sql.append("INNER JOIN users ");
    		sql.append("ON messages.user_id = users.id ");
    		sql.append("WHERE category LIKE ? ");
    		sql.append("ORDER BY created_date DESC");

    		ps = connection.prepareStatement(sql.toString());
    		ps.setString(1, "%" + search + "%");

    		ResultSet rs = ps.executeQuery();
    		List<UserMessage> ret = toUserMessageList(rs);
    		return ret;
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public List<UserMessage> getSearchDay(Connection connection, String searchDay, String searchDay2){

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT ");
    		sql.append("messages.id as id, ");
    		sql.append("messages.title as title, ");
    		sql.append("messages.text as text, ");
    		sql.append("messages.category as category, ");
    		sql.append("messages.user_id as user_id, ");
    		sql.append("messages.message_state as message_state, ");
    		sql.append("users.login_id as login_id, ");
    		sql.append("users.name as name, ");
    		sql.append("messages.created_date as created_date ");
    		sql.append("FROM messages ");
    		sql.append("INNER JOIN users ");
    		sql.append("ON messages.user_id = users.id ");
    		sql.append("WHERE messages.created_date >= ? ");
    		sql.append("AND messages.created_date <= ? ");
    		sql.append("ORDER BY created_date DESC");

    		ps = connection.prepareStatement(sql.toString());
    		ps.setString(1, searchDay);
    		ps.setString(2, searchDay2 + " 23:59:59");

    		ResultSet rs = ps.executeQuery();
    		List<UserMessage> ret = toUserMessageList(rs);
    		return ret;
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

}