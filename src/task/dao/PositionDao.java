package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import task.beans.Positions;
import task.exception.SQLRuntimeException;

public class PositionDao {

	public List<Positions> getPosition(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("position_number, ");
			sql.append("position_name ");
			sql.append("FROM positions ");
			sql.append("ORDER BY position_number ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Positions> ret = toPositionsList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Positions> toPositionsList(ResultSet rs) throws SQLException {

		List<Positions> ret = new ArrayList<Positions>();
		try {
			while(rs.next()) {
				int positionNumber = rs.getInt("position_number");
				String positionName = rs.getString("position_name");

				Positions positions = new Positions();
				positions.setPositionNumber(positionNumber);
				positions.setPositionName(positionName);

				ret.add(positions);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
