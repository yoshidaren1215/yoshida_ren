package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import task.beans.Message;
import task.exception.NoRowsUpdatedRuntimeException;
import task.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("user_id, ");
            sql.append("title, ");
            sql.append("text, ");
            sql.append("category");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", message_state");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); //title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?"); //message_state
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            System.out.println("fdsfgsdg");

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getTitle());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());
            ps.setInt(5, 0);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void deleteMessage(Connection connection, int messageId) {

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE messages SET ");
    		sql.append("message_state = ? ");
    		sql.append("WHERE ");
    		sql.append("id = ? ");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setInt(1, 1);
    		ps.setInt(2, messageId);

    		int count = ps.executeUpdate();
    		if (count == 0) {
    			throw new NoRowsUpdatedRuntimeException();
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }
}