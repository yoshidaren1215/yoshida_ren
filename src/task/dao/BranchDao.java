package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import task.beans.Branchs;
import task.exception.SQLRuntimeException;

public class BranchDao {

	public List<Branchs> getBranch(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("branch_nunber, ");
			sql.append("branch_name ");
			sql.append("FROM branchs ");
			sql.append("ORDER BY branch_nunber ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branchs> ret = toBranchsList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branchs> toBranchsList(ResultSet rs) throws SQLException {

		List<Branchs> ret = new ArrayList<Branchs>();
		try {
			while(rs.next()) {
				int branchNumber = rs.getInt("branch_nunber");
				String branchName = rs.getString("branch_name");

				Branchs branchs = new Branchs();
				branchs.setBranchNumber(branchNumber);
				branchs.setBranchName(branchName);

				ret.add(branchs);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
