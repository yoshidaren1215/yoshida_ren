package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import task.beans.UserComment;
import task.exception.SQLRuntimeException;

public class UserMessageCommentDao {

	public List<UserComment> getUserComment(Connection connection, int num){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comment.id as id, ");
			sql.append("comment.description as description, ");
			sql.append("comment.user_id as user_id, ");
			sql.append("comment.messages_id as messages_id, ");
			sql.append("comment.comment_state as comment_state, ");
			sql.append("users.name as name, ");
			sql.append("comment.created_date as created_date ");
			sql.append("FROM comment ");
			sql.append("INNER JOIN users ");
			sql.append("ON comment.user_id = users.id ");
			sql.append("INNER JOIN messages ");
			sql.append("ON comment.messages_id = messages.id ");
			sql.append("ORDER BY created_date limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs) throws SQLException{

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int messagesId = rs.getInt("messages_id");
				String description = rs.getString("description");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int commentState = rs.getInt("comment_state");

				UserComment comment = new UserComment();
				comment.setName(name);
				comment.setId(id);
				comment.setUserId(userId);
				comment.setMessagesId(messagesId);
				comment.setDescription(description);
				comment.setCreatedDate(createdDate);
				comment.setCommentState(commentState);

				ret.add(comment);
			}
			return ret;
		}finally {
			close(rs);
		}
	}
}
