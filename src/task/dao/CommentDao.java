package task.dao;

import static task.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import task.beans.Comment;
import task.exception.NoRowsUpdatedRuntimeException;
import task.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comment ( ");
            sql.append("user_id");
            sql.append(", messages_id");
            sql.append(", description");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", comment_state");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // messages_id
            sql.append(", ?"); // description
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getUserId());
            ps.setInt(2, comment.getMessagesId());
            ps.setString(3, comment.getDescription());
            ps.setInt(4, 0);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void deleteComment(Connection connection, int commentId) {

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE comment SET ");
    		sql.append("comment_state = ? ");
    		sql.append("WHERE ");
    		sql.append("id = ? ");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setInt(1, 1);
    		ps.setInt(2, commentId);

    		int count = ps.executeUpdate();
    		if (count == 0) {
    			throw new NoRowsUpdatedRuntimeException();
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

}