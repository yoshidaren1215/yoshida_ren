package task.service;

import static task.utils.CloseableUtil.*;
import static task.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import task.beans.Comment;
import task.beans.UserComment;
import task.dao.CommentDao;
import task.dao.UserMessageCommentDao;

public class CommentService {

    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserComment> getComment() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserMessageCommentDao commentDao = new UserMessageCommentDao();
    		List<UserComment> ret = commentDao.getUserComment(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void deleteComment(int commentId) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		CommentDao commentDao = new CommentDao();
    		commentDao.deleteComment(connection, commentId);

    		commit(connection);
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}