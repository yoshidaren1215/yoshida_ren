package task.service;

import static task.utils.CloseableUtil.*;
import static task.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import task.beans.Positions;
import task.dao.PositionDao;

public class PositionService {

	private static final int LIMIT_NUM = 1000;

	public List<Positions> getPositions(){

		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();
			List<Positions> ret = positionDao.getPosition(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
