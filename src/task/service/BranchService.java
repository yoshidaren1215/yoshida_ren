package task.service;

import static task.utils.CloseableUtil.*;
import static task.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import task.beans.Branchs;
import task.dao.BranchDao;

public class BranchService {

    private static final int LIMIT_NUM = 1000;

    public List<Branchs> getBranchs() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		BranchDao branchsDao = new BranchDao();
    		List<Branchs> ret = branchsDao.getBranch(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}