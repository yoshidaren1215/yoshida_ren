package task.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import task.beans.UserComment;
import task.beans.UserMessage;
import task.service.CommentService;
import task.service.MessageService;

@WebServlet(urlPatterns = {"/searchDay"})
public class SearchDayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String searchDay = request.getParameter("searchDay");
		String searchDay2 = request.getParameter("searchDay2");

		if (StringUtils.isEmpty(searchDay) && StringUtils.isEmpty(searchDay2)) {
			List<UserMessage> messages = new MessageService().getMessage();
			List<UserComment> comment = new CommentService().getComment();

			request.setAttribute("messages", messages);
			request.setAttribute("comment", comment);
		} else {
			
			MessageService messageService = new MessageService();
			List<UserMessage> searchDays = messageService.getSearchDay(searchDay, searchDay2);
			List<UserComment> comment = new CommentService().getComment();

			request.setAttribute("messages", searchDays);
			request.setAttribute("comment", comment);

			request.setAttribute("searchDay", searchDay);
			request.setAttribute("searchDay2", searchDay2);
		}
		Calendar cl = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		String today = sdf.format(cl.getTime());
		request.setAttribute("today", today);

		request.getRequestDispatcher("/top.jsp").forward(request,response);
	}

}
