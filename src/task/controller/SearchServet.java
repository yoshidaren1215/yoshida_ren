package task.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import task.beans.UserComment;
import task.beans.UserMessage;
import task.service.CommentService;
import task.service.MessageService;

@WebServlet(urlPatterns = {"/search"})
public class SearchServet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String search = request.getParameter("search");

		MessageService messageService = new MessageService();
		List<UserMessage> searchMessage = messageService.getCategory(search);
		List<UserComment> comment = new CommentService().getComment();

		request.setAttribute("messages", searchMessage);
		request.setAttribute("comment", comment);
		request.setAttribute("searchword", search);

		Calendar cl = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		String today = sdf.format(cl.getTime());
		request.setAttribute("today", today);
		request.setAttribute("searchDay2", today);
		request.setAttribute("searchDay", today);

		request.getRequestDispatcher("/top.jsp").forward(request,response);
	}

}
