package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import task.beans.Comment;
import task.beans.User;
import task.service.CommentService;

@WebServlet(urlPatterns = {"/newComment"})
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		Comment comment = getComment(request);
		if(isValid(request,messages) == true) {

			User user = (User) session.getAttribute("loginUser");
			comment.setUserId(user.getId());

			new CommentService().register(comment);
			response.sendRedirect("top");
		}else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("comment", comment);
			response.sendRedirect("top");
		}

	}

	private Comment getComment(HttpServletRequest request)throws IOException, ServletException {

		Comment comment = new Comment();
		comment.setDescription(request.getParameter("comment"));
		int messageId = Integer.parseInt(request.getParameter("message_id"));
		comment.setMessagesId(messageId);

		return comment;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isEmpty(comment) == true) {
			messages.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			messages.add("コメントは500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

}
