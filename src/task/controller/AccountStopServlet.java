package task.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import task.beans.User;
import task.service.UserService;

@WebServlet(urlPatterns = {"/accountStop"})
public class AccountStopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User stopUser = new User();
		stopUser.setId(Integer.parseInt(request.getParameter("id")));
		int accountStop = 1;
		stopUser.setAccountState(accountStop);

		new UserService().restart(stopUser);

		response.sendRedirect("management");
	}

}
