package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import task.beans.UserBranchPosition;
import task.beans.User;
import task.service.UserService;

@WebServlet(urlPatterns = {"/management"})
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		
		User loginUser = (User) session.getAttribute("loginUser");
		User checkUser = new UserService().getUser(loginUser.getId());
		if(checkUser.getBranch() == 1 && checkUser.getPosition() == 1) {
			List<UserBranchPosition> management = new UserService().getUBP();
			request.setAttribute("management", management);

			request.getRequestDispatcher("management.jsp").forward(request,response);
		} else {
			List<String> messages = new ArrayList<String>();
			messages.add("あなたには権限がありません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("top");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));

		UserService userService = new UserService();
		User user = userService.getUser(id);

		HttpSession session = request.getSession();

		session.setAttribute("settingUser", user);
		response.sendRedirect("setting");

	}

}
