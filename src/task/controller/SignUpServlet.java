package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import task.beans.Branchs;
import task.beans.Positions;
import task.beans.User;
import task.service.BranchService;
import task.service.PositionService;
import task.service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		User loginUser = (User) session.getAttribute("loginUser");
		User checkUser = new UserService().getUser(loginUser.getId());
		if(checkUser.getBranch() == 1 && checkUser.getPosition() == 1) {
			List<Branchs> branchs = new BranchService().getBranchs();
			List<Positions> positions = new PositionService().getPositions();

			request.setAttribute("branchs", branchs);
			request.setAttribute("positions", positions);

			request.getRequestDispatcher("signup.jsp").forward(request,response);
		} else {
			List<String> messages = new ArrayList<String>();
			messages.add("あなたには権限がありません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("top");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		List<Branchs> branchs = new BranchService().getBranchs();
		List<Positions> positions = new PositionService().getPositions();

		HttpSession session = request.getSession();
		User user = getUser(request);

		if(isValid(request,messages) == true) {

			new UserService().register(user);
			response.sendRedirect("top");
		} else {
			session.setAttribute("errorMessages",messages);
			request.setAttribute("user", user);
			request.setAttribute("branchs", branchs);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}
	private User getUser(HttpServletRequest request)throws IOException, ServletException {

		User user = new User();
		user.setLoginId(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		int branchNumber = Integer.parseInt(request.getParameter("branch"));
		user.setBranch(branchNumber);
		int positionNumber = Integer.parseInt(request.getParameter("position"));
		user.setPosition(positionNumber);
		int accountState = 0;
		user.setAccountState(accountState);

		return user;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("password_check");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));


		if(StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if(loginId.matches("[a-zA-Z0-9]{6,20}") == false) {
			messages.add("ログインIDは半角英数字の6文字以上20文字以下で入力してください");
		}
		if(StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else if(password.matches("[-_@+*;:#$%&A-Za-z0-9]{6,20}") == false) {
			messages.add("パスワードは記号を含む半角文字の6文字以上20文字以下で入力してください");
		}
		if(StringUtils.isEmpty(passwordCheck) == true) {
			messages.add("確認用パスワードを入力してください");
		}
		if(password.equals(passwordCheck) == false) {
			messages.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if(StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		}
		if(name.length() > 10 == true) {
			messages.add("名前は10文字以下で入力してください");
		}
		if(branch == 1 && position >= 3) {
			messages.add("所属と役職の組み合わせが正しくありません");
		}
		if(branch >= 2 && position <= 2) {
			messages.add("所属と役職の組み合わせが正しくありません");
		}
		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}
}
