package task.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import task.beans.User;
import task.service.UserService;

@WebServlet(urlPatterns = {"/accountStart"})
public class AccountStartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User startUser = new User();
		startUser.setId(Integer.parseInt(request.getParameter("id")));
		int accountStart = 0;
		startUser.setAccountState(accountStart);

		new UserService().restart(startUser);

		response.sendRedirect("management");
	}

}
