package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import task.beans.Message;
import task.beans.User;
import task.service.MessageService;

@WebServlet(urlPatterns = {"/newMessage"})
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request,response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		Message message = getMessage(request);
		if(isValid(request,messages) == true) {
			User user = (User) session.getAttribute("loginUser");
			message.setUserId(user.getId());

			new MessageService().register(message);
			response.sendRedirect("top");
		}else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("message.jsp").forward(request,response);
		}
	}

	private Message getMessage(HttpServletRequest request)throws IOException, ServletException {

		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setText(request.getParameter("text"));
		message.setCategory(request.getParameter("category"));

		return message;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String title = request.getParameter("title");
		String message = request.getParameter("text");
		String category = request.getParameter("category");

		if(StringUtils.isEmpty(title) == true) {
			messages.add("件名を入力してください");
		}
		if(30 < title.length()) {
			messages.add("件名は30文字以下で入力してください");
		}
		if(StringUtils.isEmpty(message) == true) {
			messages.add("メッセージを入力してください");
		}
		if(1000< message.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if(StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if(10 < category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

}
