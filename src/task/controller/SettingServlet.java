package task.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import task.beans.Branchs;
import task.beans.Positions;
import task.beans.User;
import task.service.BranchService;
import task.service.PositionService;
import task.service.UserService;

@WebServlet(urlPatterns = {"/setting"})
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		User loginUser = (User) session.getAttribute("loginUser");
		User checkUser = new UserService().getUser(loginUser.getId());
		List<Branchs> branchs = new BranchService().getBranchs();
		List<Positions> positions = new PositionService().getPositions();

		if(checkUser.getBranch() == 1 && checkUser.getPosition() == 1) {
			User settingUser = (User) session.getAttribute("settingUser");
			if(settingUser != null) {
				User editUser = new UserService().getUser(settingUser.getId());
				request.setAttribute("editUser", editUser);
			}
			request.setAttribute("branchs", branchs);
			request.setAttribute("positions", positions);

			request.getRequestDispatcher("settings.jsp").forward(request, response);
		} else {
			List<String> messages = new ArrayList<String>();
			messages.add("あなたには権限がありません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("top");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		List<Branchs> branchs = new BranchService().getBranchs();
		List<Positions> positions = new PositionService().getPositions();

		HttpSession session = request.getSession();

		User editUser = getEditUser(request);

		String password = request.getParameter("password");

		if(StringUtils.isEmpty(password) == true) {

			if(isValid2(request,messages) == true) {
				new UserService().updateNotPassword(editUser);

				response.sendRedirect("management");
			} else {
				session.setAttribute("errorMessages", messages);
				request.setAttribute("branchs", branchs);
				request.setAttribute("positions", positions);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}
		} else if(StringUtils.isEmpty(password) == false ) {
			editUser.setPassword(request.getParameter("password"));

			if(isValid(request,messages) == true) {
				new UserService().update(editUser);

				response.sendRedirect("management");
			} else {
				session.setAttribute("errorMessages", messages);
				request.setAttribute("branchs", branchs);
				request.setAttribute("positions", positions);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
			}
		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException{

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("login_id"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setPosition(Integer.parseInt(request.getParameter("position")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("password_check");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));

		if(StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if(loginId.matches("[a-zA-Z0-9]{6,20}") == false) {
			messages.add("ログインIDは半角英数字の6文字以上20文字以下で入力してください");
		}
		if(password.matches("[-_@+*;:#$%&A-Za-z0-9]{6,20}") == false) {
			messages.add("パスワードは記号を含む半角文字の6文字以上20文字以下で入力してください");
		}
		if(StringUtils.isEmpty(passwordCheck) == true) {
			messages.add("確認パスワードを入力してください");
		}
		if(password.equals(passwordCheck) == false) {
			messages.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if(StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		}
		if(name.length() > 10 == true) {
			messages.add("名前は10文字以下で入力してください");
		}
		if(branch == 1 && position >= 3) {
			messages.add("所属と役職の組み合わせが正しくありません");
		}
		if(branch >= 2 && position <= 2) {
			messages.add("所属と役職の組み合わせが正しくありません");
		}
		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

	private boolean isValid2(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("login_id");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("password_check");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));

		if(StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if(loginId.matches("[a-zA-Z0-9]{6,20}") == false) {
			messages.add("ログインIDは半角英数字の6文字以上20文字以下で入力してください");
		}
		if(StringUtils.isEmpty(name)) {
			messages.add("名前を入力してください");
		}
		if(name.length() > 10 == true) {
			messages.add("名前は10文字以下で入力してください");
		}
		if(password.equals(passwordCheck) == false) {
			messages.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if(branch == 1 && position >= 3) {
			messages.add("所属と役職の組み合わせが正しくありません");
		}
		if(branch >= 2 && position <= 2) {
			messages.add("所属と役職の組み合わせが正しくありません");
		}
		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}
}
