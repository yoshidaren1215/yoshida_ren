<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div class="form-area">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="messages">
						<li><c:out value="${messages}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="newMessage" method="post">
			新規投稿<br />
			<label for="title">件名</label>
			<input type="text" name="title" value="${message.title}" size="30" maxlength="50" /><br />

			<textarea name="text" cols="100" rows="10" class="tweet-box"><c:out value="${message.text}" /></textarea>
			(1000文字まで)<br />
			<label for="category">カテゴリー</label>
			<input type="text" name="category" value="${message.category}" size="30" maxlength="30" />
			<br />
			<input type="submit" value="投稿" />
			<a href="top">戻る</a>
		</form>
	</div>
	<div class="copyright"> Copyright(c)Yoshida Ren</div>
</body>
</html>