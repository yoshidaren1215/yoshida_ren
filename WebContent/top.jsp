<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="./">ログイン</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="newMessage">新規投稿</a>
				<a href="management">ユーザー管理</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<c:if test="${not empty loginUser}">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="login_id">
					@
					<c:out value="${loginUser.loginId}" />
				</div>
			</div>
		</c:if>

		<form action="search" method="post">
			<input name="search" id="search" value="${searchword}" /> <input
				type="submit" value="カテゴリー検索" /><br />
		</form>
		<form action="searchDay" method="post">
			<input type="date" name="searchDay" value="${searchDay}"
				max="${today}" />～ <input type="date" name="searchDay2"
				value="${searchDay2}" max="${today}" /> <input type="submit"
				value="期間検索">
		</form>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<c:if test="${message.messageState == 0}">
					<div class="message">
						<div class="title">
							<label for="title">件名：</label>
							<c:out value="${message.title}" />
						</div>
						<div class="text">
							<label for="text">本文：</label>
							<c:out value="${message.text}" />
						</div>
						<div class="category">
							<label for="category">カテゴリー：</label>
							<c:out value="${message.category}" />
						</div>
						<div class="account-name">
							<span class="name"> <label for="name">投稿者：</label> <c:out
									value="${message.name}" />
							</span>
						</div>
						<div class="date">
							<label for="date">投稿日時：</label>
							<fmt:formatDate value="${message.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>
						<c:if test="${message.userId == loginUser.id}">
							<form action="deleteMessage" method="post">
								<input name="deleteMessage" value="${message.id}" type="hidden" />
								<input type="submit" value="投稿削除"
									onclick="return confirm('投稿を削除しますか？')" />
							</form>
						</c:if>

						<c:forEach items="${comment}" var="comment">
							<c:if
								test="${message.id == comment.messagesId and comment.commentState == 0}">
								<div class="text">
									<label for="">コメント</label><br />
									<c:out value="${comment.description}" />
								</div>
								<span class="name"> <label for="name">投稿者：</label> <c:out
										value="${comment.name}" />
								</span>
								<div class="date">
									<label for="date">投稿日時：</label>
									<fmt:formatDate value="${comment.createdDate}"
									
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<c:if test="${comment.userId == loginUser.id}">
									<form action="deleteComment" method="post">
										<input name="deleteComment" value="${comment.id}"
											type="hidden" /> <input type="submit" value="コメント削除"
											onclick="return confirm('コメントを削除しますか？')" />
									</form>
								</c:if>
							</c:if>
						</c:forEach>
						<div class="comment-area">
							<form action="newComment" method="post">
								<input name="message_id" value="${message.id}" type="hidden" />
								<textarea name="comment" cols="100" rows="5" class="comment-box"></textarea>
								<br /> <input type="submit" value="コメントする">
							</form>
						</div>
					</div>
				</c:if>
			</c:forEach>
		</div>
		<div class="copyright">Copyright(c)Yoshida Ren</div>
	</div>
</body>
</html>