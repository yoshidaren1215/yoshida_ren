<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="message">
						<li><c:out value="${ message }" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br /><label for="login_id">ログインID</label><br />
			<input name="login_id" id="login_id" value="${user.loginId}" /><br />

			<label for="password">パスワード</label><br />
			<input name="password" type="password" id="password" /><br />

			<label for="password_check">確認用パスワード</label><br />
			<input name="password_check" type="password" id="password_check"><br />

			<label for="name">名前</label><br />
			<input name="name" id="name" value="${user.name}" />(名前は公開プロフィールに表示されます)<br />

			<label for="branch">所属支店</label><br />
			<select name="branch" id="branch">
				<c:forEach items="${branchs}" var="branchs" >
					<c:choose>
						<c:when test="${branchs.branchNumber == user.branch}">
							<option value="${branchs.branchNumber}" selected>${branchs.branchName}</option>
						</c:when>
						<c:otherwise>
							<option value="${branchs.branchNumber}">${branchs.branchName}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br />

			<label for="position">部署・役職</label><br />
			<select name="position" id="position" >
				<c:forEach items="${positions}" var="positions" >
					<c:choose>
						<c:when test="${positions.positionNumber == user.position}">
							<option value="${positions.positionNumber}" selected>${positions.positionName}</option>
						</c:when>
						<c:otherwise>
							<option value="${positions.positionNumber}" >${positions.positionName}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select><br />

			<input type="submit" value="登録" /><br />
			<a href="management">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Yoshida Ren</div>
	</div>
</body>
</html>