
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<table>
		<tr>
			<th>ID</th><th>ログインID</th><th>名前</th><th>所属支店</th><th>役職</th><th>編集</th><th>復活／停止</th>
		</tr>
		<c:forEach items="${management}" var="management">
			<tr>
				<td>
					<span class="id"><c:out value="${management.id}" /></span>
				</td>
				<td>
					<span class="loginid"><c:out value="${management.loginId}" /></span>
				</td>
				<td>
					<span class="name"><c:out value="${management.name}" /></span>
				</td>
				<td>
					<span class="branch"><c:out value="${management.branchName}" /></span>
				</td>
				<td>
					<span class="position"><c:out value="${management.positionName}" /></span>
				</td>
				<td>
					<form action="management" method="post">
						<input name="id" id="id" value="${management.id}" type="hidden" />
						<input type="submit" value="編集" />
					</form>
				</td>
				<td>
					<c:choose>
						<c:when test="${management.accountState == 0 }">
							<form action="accountStop" method="post">
								<input name="id" id="id" value="${management.id}" type="hidden" />
								<input type="submit" value="停止" onclick="return confirm('アカウントを停止しますか？')" />
							</form>
						</c:when>
						<c:otherwise>
							<form action="accountStart" method="post" onsubmit="return stateStart()">
								<input name="id" id="id" value="${management.id}" type="hidden" />
								<input type="submit" value="復活" />
							</form>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</c:forEach>
	</table>
	<br /><a href="signup">新規登録</a>
	<a href="top">戻る</a><br />
	<div class="copyright">Copyright(c)Yoshida Ren</div>
	<script>
		function stateStart() {
			var start = window.confirm('アカウントを復活しますか？');
			if(start){
				return true;
			}else{
				return false;
			}
		}
	</script>
</body>
</html>